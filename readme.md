## project info:
scripts container in this project help to compile gradle projects in ci environment ( jenkins ) this project is compiled into a
docker image which when run will copy all the scripts into a volume-shared `target` dir, to make it works import this way the scripts 
 in your project, then read instruction on how to use it .. ( 2 commands are required, and docker as dependency )



### TEST THE BUILD
```bash
## build a test image 
./src/buildCurrentVersionName.sh
DOCKER_REPO="test" PUSH_SHA=0 PUSH_LATEST=0 PUSH_TAG=0 IMAGENAME="ci-gradlebuilder" DOCKERFILE_DIR=`pwd` ./ci/buildImage.sh
## if build is success, try importing scripts from that image:
mkdir /tmp/testci-gradlebuilder
cd /tmp/testci-gradlebuilder

OWNERID=$(stat -c '%u' ./)
docker run --rm -ti  -e OWNERID=${OWNERID} -v `pwd`/ci:/target test/ci-gradlebuilder:latest
ls -la 

```



## build this project image (ci-gradlebuilder) using (ci-imagebuilder)   
use the `ci-imagebuilder` image which is a generic image builder scripts 
to build the docker image with those scripts
so:
```bash
# from project root
 
## get updated ci scripts
docker pull quay.io/fravi/ci-imagebuilder:latest
docker run --rm -ti  -v `pwd`/ci:/target quay.io/fravi/ci-imagebuilder:latest

## generate env 
./ci/buildCurrentVersionName.sh

## build and push 
DOCKER_REPO="quay.io/fravi"  IMAGENAME="ci-gradlebuilder" \
DOCKER_BUILD_OPTS="--compress" PUSH_SHA=0 PUSH_LATEST=1 PUSH_TAG=1 DOCKERFILE_DIR=`pwd` ./ci/buildImage.sh

```

to download the build scripts in `ci` dir in your project use :
```bash 
docker run --rm -ti  -v `pwd`/ci:/target quay.io/fravi/ci-gradlebuilder:latest
```
this will copy the script from the docker image to the "ci" dir in your project ( there is a default cmd which use rsync ( without deletion ) )

## import scripts of this project to your project:
from project root 
```bash
docker pull quay.io/fravi/ci-gradlebuilder:latest 
docker run --rm -ti  -v `pwd`/ci:/target quay.io/fravi/ci-gradlebuilder:latest
```


## BUGS:
at the moment there is a bug that wrongly recognize the branch when has just been merged ( if no commit happened read the old branch which has been merged from )
