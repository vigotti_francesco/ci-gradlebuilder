FROM alpine
RUN apk update
RUN apk add rsync openssh bash


ADD dockersrc/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh


ADD src/ /src/
WORKDIR /src/


CMD [ "/bin/bash", "/entrypoint.sh" ]