#!/usr/bin/env bash

RSYNC_OPTS=""

if [ ! -z "${OWNERID}" ]; then
  RSYNC_OPTS="${RSYNC_OPTS} --chown=${OWNERID}:${OWNERID}"
fi

exec rsync -avz ${RSYNC_OPTS} /src/ /target/
