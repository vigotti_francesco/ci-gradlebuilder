#!/usr/bin/env bash
set -e



export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )
export BASENAME=$(basename $PROJECT_DIR)


. $SCRIPT_DIR/05_SHARED_LIB.sh


echo " THIS SCRIPT WILL PREPARE LOCAL ENV FOR EXECUTION OF THOSE CI SCRIPTS IN DEV-ENVIRONMENTS ( DEVELOPER PC )"
echo "Because on ci env, there are specific dirs to be used as cache for the builds"
echo "in order to avoid conflicts with current dev, maven and gradle configuration must be exported to these temp directories.."
echo ""
echo "*** [ADDITIONAL NOTES]:"
echo "be aware that org.gradle.java.home in gradle.properties could create problems.. remote it from the created file after execution of this script!"

TMP_M2_DIR=${HOME}/tmp_docker_mnt/.m2
TMP_GRADLE_DIR=${HOME}/tmp_docker_mnt/.gradle

if [ ! -d "$TMP_GRADLE_DIR" -o ! -d "$TMP_M2_DIR" ]; then
read -p "initialize $TMP_GRADLE_DIR && $TMP_M2_DIR ? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    set -xe
    mkdir -p "$TMP_M2_DIR"
    cp -a "${HOME}/.m2/settings.xml" "$TMP_M2_DIR/settings.xml"

    mkdir -p "$TMP_GRADLE_DIR"
    cp -a "${HOME}/.gradle/gradle.properties" "$TMP_GRADLE_DIR/gradle.properties"

    set +x
else
 echo "choosed no, skipping"
 exit 1
fi

else
echo "temp dir already initialized"

fi

