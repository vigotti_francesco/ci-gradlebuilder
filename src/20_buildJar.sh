#!/usr/bin/env bash
# this script will build into /usr/bin/app/build

# run with:
# buildJar.sh
# or
# buildJar.sh test build ...

set -x

export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )

cd $PROJECT_DIR
echo "[start] 20_buildJar.sh"
# read home directory or set to a tmp path
if [ -z "$HOME" ]; then
  export HOME="/tmp/tmphome"
fi

## includes
. $SCRIPT_DIR/05_SHARED_LIB.sh

DOCKER_IMAGE_GRADLE=${DOCKER_IMAGE_GRADLE:-"quay.io/fravi/docker-gradle:master"}

export GRADLE_CACHE_ENABLED=${GRADLE_CACHE_ENABLED:-1} # by default enable gradle cache
export MAVEN_CACHE_ENABLED=${MAVEN_CACHE_ENABLED:-1} # by default enable gradle maven
export MAVEN_CACHE_DIR=${MAVEN_CACHE_DIR:-"${HOME}/tmp_docker_mnt/.m2"} ## which is default root gradle cache dir
export GRADLE_CACHE_DIR=${GRADLE_CACHE_DIR:-"${HOME}/tmp_docker_mnt/.gradle"} ## which is default root gradle cache dir
export DOCKER_RUN_OPTIONS=${DOCKER_RUN_OPTIONS:-"--network=host"} ## can be used to inject further options ( such additional volumes .. )
export DOCKER_BUILDCMD=${DOCKER_BUILDCMD:-"rm -Rf /app/build/* && gradle dist && sync"} ## can be used to inject further options ( such additional tests, different build command/options .. )
export DOCKER_POST_BUILDCMD=${DOCKER_POST_BUILDCMD:-""} ## POST BUILD CMD can be injected using .build_env , ( ie for publishing packages )
export RESET_PROJECT_CACHE=${RESET_PROJECT_CACHE:-0} ## if 1 will reset cache of downloaded modules
export OPT_RUN_INTERACTIVE=${OPT_RUN_INTERACTIVE:-"n"} ## if y will run docker interactively ( to allow ctrl+c from the terminal when run locally )
export CONF_ADDITIONAL_MOUNTS=${CONF_ADDITIONAL_MOUNTS:-""} ## allow injection of additional mounts , ie : gradle.properties


### ALWAYS LOWERCASE THE PROJECT NAME ( when is taken from the project directory)
if [ -z "$PROJECT_NAME" ]; then
  export PROJECT_NAME=$(basename `pwd` | tr '[:upper:]' '[:lower:]')
fi

CUR_USER_HOME="$HOME" # because --userns=keep-id   now use same user as current
GRADLE_USER_HOME="$CUR_USER_HOME/.gradle"
if [[ ${GRADLE_CACHE_ENABLED} -eq 1 ]]; then
  export  GRADLE_CACHE_VOLUME='-v '${GRADLE_CACHE_DIR}":$GRADLE_USER_HOME:rw"
  mkdir -p "${GRADLE_CACHE_DIR}"
fi

if [[ ${MAVEN_CACHE_ENABLED} -eq 1 ]]; then
  export MAVEN_CACHE_VOLUME='-v '${MAVEN_CACHE_DIR}":$CUR_USER_HOME/.m2:rw"
  mkdir -p "${MAVEN_CACHE_DIR}"
fi




docker pull $DOCKER_IMAGE_GRADLE


export USERID=`id -u`

#### notes :
## - --net=host is necessary with some docker network plugins ( ie weave )
#if [ -d "${PROJECT_DIR}/build" ]; then
#  echo "delete build dir, because build dir is mounted in docker as rw volume cannot be reset by gradle with -clean- task"
#fi

# new docker version doesn't mount unexistent directories
mkdir -p "${PROJECT_DIR}/.gradle"
mkdir -p "${PROJECT_DIR}/build"

cmdOptsRunTI="-t"
if [[  "${OPT_RUN_INTERACTIVE}" =~ ^[Yy]$ ]]; then ## enter using rootg
cmdOptsRunTI="-ti"
fi

podman run --userns=keep-id --group-add keep-groups --rm $cmdOptsRunTI ${DOCKER_NETOPTIONS:-}\
 --entrypoint="bash" \
 -e "HOME=$HOME" \
 -e "GRADLE_USER_HOME=$GRADLE_USER_HOME" \
 ${GRADLE_CACHE_VOLUME} \
 ${MAVEN_CACHE_VOLUME} \
 -v "${PROJECT_DIR}:/app/:ro" \
 -v "${PROJECT_DIR}/.gradle:/app/.gradle:rw" \
 -v "${PROJECT_DIR}/build:/app/build:rw" \
  $CONF_ADDITIONAL_MOUNTS \
 -w="/app/" \
 $DOCKER_IMAGE_GRADLE -c "$DOCKER_BUILDCMD"
 #$DOCKER_IMAGE_GRADLE -c "env "

## this is no longer necessary because podman can use --userns=keep-id   and keep same user
#podman  run --userns=keep-id --rm -t ${DOCKER_NETOPTIONS:-}\
# -v "${PROJECT_DIR}/.gradle:/app/.gradle:rw" \
# -v "${PROJECT_DIR}/build:/app/build:rw" \
# -w="/app/" \
# ubuntu bash -c 'chown -R '${USERID}':'${USERID}' /app/build && chown -R '${USERID}':'${USERID}'  /app/.gradle'

if [ ! -z "$DOCKER_POST_BUILDCMD" ]; then
echo "post build cmd detected : $DOCKER_POST_BUILDCMD , executing.."
podman run --userns=keep-id --group-add keep-groups --rm $cmdOptsRunTI ${DOCKER_NETOPTIONS:-}\
 --entrypoint="bash" \
 -e "HOME=$HOME" \
  -e "GRADLE_USER_HOME=$GRADLE_USER_HOME" \
 ${GRADLE_CACHE_VOLUME} \
 ${MAVEN_CACHE_VOLUME} \
 -v "${PROJECT_DIR}:/app/:ro" \
 -v "${PROJECT_DIR}/.gradle:/app/.gradle:rw" \
 -v "${PROJECT_DIR}/build:/app/build:rw" \
  $CONF_ADDITIONAL_MOUNTS \
 -w="/app/" \
 $DOCKER_IMAGE_GRADLE -c "$DOCKER_POST_BUILDCMD"
fi
