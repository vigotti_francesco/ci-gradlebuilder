#!/usr/bin/env bash
# autoupdate scripts in this directory
# it simply executed the suggested update commands written in readme.md

set -x

export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )

## ensure that the script is not run from a unknown location ( because I'm going to overwrite the "../ci" dir )
SCRIPT_CURDIRNAME=${SCRIPT_DIR##*/}
if [ "$SCRIPT_CURDIRNAME" != "ci" ]; then
  echo "this script can be run only if locaed inside a dir named 'ci' "
  exit 1
fi


# override ci configuration for this self update
if [  -f "$PROJECT_DIR/_ci_override.sh" ]; then
  echo "ci override configuration found, loading _ci_override.sh"
  . $PROJECT_DIR/_ci_override.sh
fi

## refresh image

#FRA_CI_IMAGE_NAME_DEFAULT="quay.io/fravi/ci-gradlebuilder:master"
FRA_CI_IMAGE_NAME_DEFAULT="b-lb-repo-docker.vprod.fvigotti.net:8089/fravi/ci-gradlebuilder:master"
FRA_CI_IMAGE_NAME=${FRA_CI_IMAGE_NAME:-$FRA_CI_IMAGE_NAME_DEFAULT}
echo "FRA_CI_IMAGE_NAME=$FRA_CI_IMAGE_NAME"

## refresh image
docker pull "${FRA_CI_IMAGE_NAME}"

OWNERID=$(stat -c '%u' ./)

docker pull "${FRA_CI_IMAGE_NAME}" || { echo "[FATAL ERROR]cannot download the ci image to update : ${FRA_CI_IMAGE_NAME} "; exit 2; }

## clean ci directory
if [ -d ${PROJECT_DIR}/ci ]; then
  rm -f ${PROJECT_DIR}/ci/*
fi

docker run --rm -ti -e OWNERID=${OWNERID} -v ${PROJECT_DIR}/ci:/target "${FRA_CI_IMAGE_NAME}"


