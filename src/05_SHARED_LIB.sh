#!/usr/bin/env bash
# this script will build into /usr/bin/app/build

# run with:
# buildJar.sh
# or
# buildJar.sh test build ...

set -x

export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )

# constants
readonly export BUILDVARS_DIR="${BUILDVARS_DIR:-"$PROJECT_DIR/build_vars/"}"
readonly export TAG_NAME_TMP_FILE="${BUILDVARS_DIR}/tag.name" # this file will contain generated version name (NB: used in jenkins as project description )
readonly export BRANCH_NAME_TMP_FILE="${BUILDVARS_DIR}/branch.name" # this file will contain generated version name (NB: used in jenkins as project description )
readonly export VERSION_NAME_TMP_FILE="${BUILDVARS_DIR}/version.name" # this file will contain generated version name
readonly export BUILD_RESULTS_TMP_FILE="${BUILDVARS_DIR}/build.results" # this file will contain generated version name

## import additional build variables directly from project, if exists
if [ -f "${PROJECT_DIR}/.build_env.sh" ]; then
  echo "importing additional build env file : ${PROJECT_DIR}/.build_env.sh"
  . "${PROJECT_DIR}/.build_env.sh"
fi


## there are needed because commit will be cherry picked from jenkins plugin so easier ways to retrieve those informations do not work
getShortCommitHash(){
  echo $(git rev-parse --short HEAD)
}

getCurrentBranch(){
  #echo $(git for-each-ref --sort=-committerdate --format='%(refname:short) %(objectname)' | grep "$(git rev-parse HEAD)"  | head -1 | cut -d" " -f 1 | cut -d '/' -f 2)
  echo $(git rev-parse --abbrev-ref HEAD)
  # | grep origin
}

# ie: 20160729.153549
buildVersionDateTag(){
  date +'%Y%m%d.%H%M%S'
}

getCurrentTag(){
  git describe --exact-match --tags $(git rev-parse HEAD) 2>/dev/null || printf ''
}

getAutoTag(){
  declare -n ret=$1 # name of the return variable
  if [ -z "$TAG_AUTOTAG_BRANCH" ]; then ### if empty default to branch
   export TAG_AUTOTAG_BRANCH="$(getCurrentBranch)."
  fi
  if [ -z "$TAG_AUTOTAG_TIME" ]; then
   export TAG_AUTOTAG_TIME=$(date +"%Y-%d-%m-%H%M-%S")
  fi
  if [ -z "$TAG_AUTOTAG_POSTFIX" ]; then # if empty default to commit hash  ( it's postfix so ordering can be kept)
   export TAG_AUTOTAG_POSTFIX=".$(getShortCommitHash)"
  fi

  if [ "$TAG_AUTOTAG_PREFIX" == "-" ]; then ## field invalidated by -
    export TAG_AUTOTAG_PREFIX="" ;
  fi
  if [ "$TAG_AUTOTAG_POSTFIX" == "-" ]; then ## field invalidated by -
    export TAG_AUTOTAG_POSTFIX="" ;
  fi
  if [ "$TAG_AUTOTAG_TIME" == "-" ]; then ## field invalidated by -
    export TAG_AUTOTAG_TIME="" ;
  fi
  if [ "$TAG_AUTOTAG_BRANCH" == "-" ]; then ## field invalidated by -
    export TAG_AUTOTAG_BRANCH="" ;
  fi

  ret="${TAG_AUTOTAG_PREFIX}${TAG_AUTOTAG_BRANCH}${TAG_AUTOTAG_TIME}${TAG_AUTOTAG_POSTFIX}"
}


writeProjectTagnameDescription(){
  local -r tagname="$1"
  echo " updating tag file (${TAG_NAME_TMP_FILE}) with project tag : $tagname "
  echo "$tagname"  > "${TAG_NAME_TMP_FILE}"
}
appendBuildResults(){
 KEY=$1
 VALUE=$2
 echo ${KEY}'='$VALUE >> "$BUILD_RESULTS_TMP_FILE"
}
prependBuildResults(){
 KEY=$1
 VALUE=$2
 tempfile="${PROJECT_DIR}/tmp"
 echo ${KEY}'='$VALUE  | cat - "${BUILD_RESULTS_TMP_FILE}" > "$tempfile" && mv "$tempfile" "${BUILD_RESULTS_TMP_FILE}"

}

initBuildResultsFile(){
 echo '# build results initialized '$(date) > "$BUILD_RESULTS_TMP_FILE"
 appendBuildResults 'buildDate' "'$(date)'"
}

getBuildRepoFullDigest(){ # return the repository digest ( --> sha256:checksum <-- )
    IMAGEID=$1
    declare -n ret=$2 # name of the return variable
    ret=$(skopeo inspect --format='{{ (index .Digest ) }}'  docker://$IMAGEID)
    # ie = sha256:6039d7869977b3ff617a70f7467f6edef5a7ea2eae5dcb06f354fe9c17d252a2
}

#
#getCurrentVersion(){
#  if [ -f "${PROJECT_DIR}/VERSION" ]; then
#  head -1  "${PROJECT_DIR}/VERSION" | cut -f 1
#  else
#  printf $DEFAULT_EMPTY_VERSION
#  fi
#}

## if current branch is dev, append "-SNAPSHOT" TO VERSION-
#getCurrentBuildVersion(){
#  if [ -f "${PROJECT_DIR}/VERSION" ]; then
#  version=$(head -1  "${PROJECT_DIR}/VERSION" | cut -f 1)
#  version_append=""
#  if [ "$(getCurrentBranch)" != "master" ]; then
#    export version_append="-SNAPSHOT"
#  fi
#  echo "${version}${version_append}"
#  else
#  printf $DEFAULT_EMPTY_VERSION
#  fi
#}




