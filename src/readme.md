## gradlebuilder image

#### description:
this image provide scripts to facilitate the building of gradle projects,
the typical usage is to copy directly those scripts in your gradle project under `ci` project


## usage:
to copy/update the scripts in your $projectRoot/ci dir :

```bash

# docker tags :

# :master - to get last master
# :latest - to get last dev 
# :vxxx - to get specific version 

docker pull b-lb-repo-docker.vprod.fvigotti.net:8089/fravi/ci-gradlebuilder:master
OWNERID=$(stat -c '%u' ./)
docker run --rm -ti -e OWNERID=${OWNERID} -v `pwd`/ci:/target quay.io/fravi/ci-gradlebuilder:master
```

##### execution steps 
 - ./ci/10_getProjectVersion.sh
 - ./ci/20_buildJar.sh
 - ./ci/30_buildImage.sh

##### ./ci/20_buildJar.sh params
| ARGS                       | defined as env vars |
| -----------------------------|---|
| ${CONF_ADDITIONAL_MOUNTS:-} | allow additional mounts , ie CONF_ADDITIONAL_MOUNTS="-v /home/francesco/.gradle/gradle.properties:/home/francesco/.gradle/gradle.properties:ro |

##### ./ci/30_buildImage_v2.sh params 
| ARGS                        | defined as env vars |
| -----------------------------|---|---|
| $DOCKER_BUILD_OPTS           | additional build options, ie: DOCKER_BUILD_OPTS="--network=host --compress --squash" |
| $IMAGENAME                   | imagename ( default to directory name )  |
| $PUSH                        | push the image to repository after build ( excluding default :latest or :master if on dev/master branch , including all other tags )  |
| $PUSH_LATEST                        | push the latest image if on dev branch  |
| $PUSH_MASTER                        | push the master image to repository after build  if on master branch  |
| $DOCKERFILE                  | full path of the dockerfile, default to `$projectDir/src/Dockerfile` , fallback to `$projectDir/Dockerfile`  |  
| $REFRESH_BASEIMAGE           | default 1 , if 1 will attempt to pull the base image `FROM` from specified Dockerfile before  building |  
| $PROJECT_NAME                | default $dirname->lowercased  , if set will change the default project name ( and also built image name )   
| ${DOCKER_NETOPTIONS:-}       | when configured as env variable allow specify network options ( ie : --net=host ) for `docker run` commands       
| ${DOCKER_NETBUILD_OPTIONS:-} | the same as $DOCKER_NETOPTIONS , but used in `docker build` commands ( require docker >= 1.13 ) 
| ${OPT_RUN_INTERACTIVE:-}     | if y will run podman interactively ( useful to use ctrl+c when on dev machine ) 
 
`
IMAGENAME="histatsprocessor" ./ci/30_buildImage.sh
DOCKER_REPO_ARR="b1repo-docker.vprod.fvigotti.net:8089/fraimport,b2repo-docker.vprod.fvigotti.net:8089/fraimport"  IMAGENAME="histatsprocessor" ./ci/30_buildImage_v2.sh
`

 - build and push
`DOCKER_REPO="$REGISTRY" IMAGENAME="$IMAGENAME" PUSH=1 ./ci/30_buildImage.sh`
`DOCKER_REPO="$REGISTRY" IMAGENAME="$IMAGENAME" PUSH=1 ./ci/30_buildImage.sh`

##### deploy artifact to maven (deprecated, use only docker images now ):

export VERSION=$(cat build_vars/version.name)
docker run --rm -t --net=host  -v/etc/ssl/certs/:/etc/ssl/certs/:ro  -v "$HOME/.m2/":/root/.m2/ -v "$(pwd)/build/libs/":/data/ \
maven \
bash -c "cd /data/ ; \
mvn --batch-mode  deploy:deploy-file \
-DgroupId=net.fvigotti -DartifactId=---PROJECTNAME--- \
-Dversion=$VERSION -DgeneratePom=true -Dpackaging=jar \
-DrepositoryId=nexus -Durl=https://---MAVENREPOURL---/repository/maven-releases \
-Dfile=---PROJECTNAME----$VERSION.jar -U"



# external configuration
in project dir  a _ci_override.sh script can contain override for ci scripts automations,
- $FRA_CI_IMAGE_NAME   
  if defined can contain different ci image for selfupdate , ie quay.io/fravi/ci-gradlebuilder:latest instead of quay.io/fravi/ci-gradlebuilder:master
