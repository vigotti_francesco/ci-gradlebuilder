#!/usr/bin/env bash
set -xe



export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )
export BASENAME=$(basename $PROJECT_DIR)


. $SCRIPT_DIR/05_SHARED_LIB.sh

## ACCEPTED ENV VARIABLES :
export DOCKER_REPO=${DOCKER_REPO:-test}

export PUSH=${PUSH:-0} # IF 1 , image will be pushed to docker repository
export IMAGENAME=${IMAGENAME:-$BASENAME} # IF 1 , image will be pushed to docker repository


export REFRESH_BASEIMAGE=${REFRESH_BASEIMAGE:-1} # IF 1 , image will be pushed to docker repository
export TAG_VERSION=$(getCurrentTag)
export TAG_AUTOTAG=${TAG_AUTOTAG:-}

echo "[INFO] default TAG_AUTOTAG = $TAG_AUTOTAG"

if [ -z "$TAG_AUTOTAG" ]; then
  getAutoTag "resultsAutotag"
   export TAG_AUTOTAG=$resultsAutotag
   echo "[INFO] generated autotag = $TAG_AUTOTAG"
fi

if [ -z "$TAG_VERSION" ]; then
   echo "[INFO] overridden tag version with TAG_AUTOTAG=$TAG_AUTOTAG"
  export  TAG_VERSION="$TAG_AUTOTAG"
fi

if [ -z "${DOCKERFILE}" ]; then
  export DOCKERFILE="${PROJECT_DIR}/src/Dockerfile" # default to /src/Dockerfile
  if [ ! -f "${DOCKERFILE}" ] && [ -f "${PROJECT_DIR}/Dockerfile" ]; then # but if not exist
    export DOCKERFILE=$PROJECT_DIR"/Dockerfile" ## default to /Dockerfile
  fi
fi

if [ ! -f "${DOCKERFILE}" ]; then
  echo "[FATAL] DOCKERFILE NOT FOUND at path : ${DOCKERFILE}"
  exit 1 ;
fi

## enter the directory of the dockerfile ( all COPY / ADD / .. command should reference from Dockerfile location )
cd $(dirname "${DOCKERFILE}")


if [ -z "${IMAGENAME}" ]; then
  echo " please provide imagename! "
  exit 1;
fi

DOCKER_BUILD_OPTS=${DOCKER_BUILD_OPTS:-} # ie : --network=host

initBuildResultsFile

function do_refresh_base_image(){
  local -r BASEIMAGE=$(cat ${DOCKERFILE}  | egrep "^FROM" | cut -d' ' -f 2)
  if [ ! -z "${BASEIMAGE}" ]; then
   docker pull "${BASEIMAGE}" || echo "[WARNING] error refreshing base image ${BASEIMAGE} before build "
  fi
}
 [[ ${REFRESH_BASEIMAGE} -eq 1 ]] && do_refresh_base_image


function do_build(){
  local -r DOCKER_REPO=$1
  local -r IMAGENAME=$2
  local -r TAG=$3
  docker build  --build-arg=COMMIT="$(getShortCommitHash)" --build-arg="BRANCH=$(getCurrentBranch)"  ${DOCKER_NETBUILD_OPTIONS:-} ${DOCKER_BUILD_OPTS} -t "${DOCKER_REPO}/${IMAGENAME}:${TAG}" -f "${DOCKERFILE}" .
  echo "[BUILT] ${DOCKER_REPO}/${IMAGENAME}:${TAG}"
}

function do_retag(){
  local -r DOCKER_REPO=$1
  local -r IMAGENAME=$2
  local -r TAG=$3
  local -r DOCKER_REPO_NEW=$4
  local -r IMAGENAME_NEW=$5
  local -r TAG_NEW=$6
  docker tag "${DOCKER_REPO}/${IMAGENAME}:${TAG}" "${DOCKER_REPO_NEW}/${IMAGENAME_NEW}:${TAG_NEW}"
  echo "[TAGGED]" ${DOCKER_REPO_NEW}/${IMAGENAME_NEW}:${TAG_NEW}
}
function do_push(){
  local -r DOCKER_REPO=$1
  local -r IMAGENAME=$2
  local -r TAG=$3

  _full_image="${DOCKER_REPO}/${IMAGENAME}:${TAG}"
  docker push $_full_image
  echo "[PUSHED]" $_full_image
  #appendBuildResults "IMAGE_TAG_NAME" "${TAG}" # ie IMAGE_TAG_NAME=latest
  #appendBuildResults "IMAGE_TAG_${TAG}_DIGEST" "$(getBuildRepoFullDigest $_full_image)" # IMAGE_TAG_latest_DIGEST=$DIGEST ( not valid because tagname could contain invalid char for variable name ( ie : '.' ))

  IMG_DIGEST=""
  getBuildRepoFullDigest $_full_image IMG_DIGEST
  appendBuildResults "IMAGE_DIGEST" "$IMG_DIGEST"
  appendBuildResults "IMAGE_NAME" "$_full_image"
}

BRANCHNAME="$(getCurrentBranch)"
builtTag=""  ## tag of the image built ( so can be used in retaggging )
## if on master branch  build :master and :v$tag
if [ "$BRANCHNAME" = "master" ]; then
 builtTag="master"
 do_build "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
 [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"



fi

## if on "dev.*" branch build :latest
if [[ "$BRANCHNAME" =~ ^dev.*$ ]]; then
#if [ "$BRANCHNAME" =~ "dev" ]; then
 builtTag="latest"
 do_build "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
 [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"

 # if git tag is defined build with :$tag
# [[ ! -z "${TAG_VERSION}" ]] && do_retag "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"
# [[ ${PUSH} -eq 1 ]] && [[ ! -z "${TAG_VERSION}" ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "dev-${TAG_VERSION}"

fi

# custom tag with push
if [[ ! -z "${TAG_VERSION}" ]]; then
     writeProjectTagnameDescription "${TAG_VERSION}"
     if [[ -z "$builtTag" ]]; then ## if not yet built with tag
       builtTag="$TAG_VERSION"
       do_build "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
       [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag" && { prependBuildResults "push $DOCKER_REPO" "ok" ; }
     else
     do_retag "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"
     [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"  && { prependBuildResults "push $DOCKER_REPO" "ok" ; }
     fi

fi

## if on label-XXX branch build :label-XXX branch ( custom branch )
# checking if branch start with .. "label-" , will create a tag with full branch name
#if [[ "$BRANCHNAME" == label-* ]]; then
# do_build "${DOCKER_REPO}" "${IMAGENAME}" "$BRANCHNAME"
# [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "$BRANCHNAME"
#
# # if git tag is defined build with :$tag
# [[ ! -z "${TAG_VERSION}" ]] && do_retag "${DOCKER_REPO}" "${IMAGENAME}" "$BRANCHNAME" "${DOCKER_REPO}" "${IMAGENAME}" "${BRANCHNAME}-${TAG_VERSION}"
# [[ ${PUSH} -eq 1 ]] && [[ ! -z "${TAG_VERSION}" ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "${BRANCHNAME}-${TAG_VERSION}"
#fi

exit 0 ; ## ensure that this is the last errorcode if the program reach this point

}
