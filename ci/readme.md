# usage:


## branch building and versioning:
- git branch `master` -> will build `:master` and tagged version if tag is present -> `:$(git current tag )`
- git branch`dev` -> will build to :latest
- git branch`label-XXX` -> will build to docker image tag : label-XXX


## prepare:
- add `build_vars/` to gitignore
- pull fresh scripts
  ```bash
  docker pull quay.io/fravi/ci-imagebuilder:latest
  OWNERID=$(stat -c '%u' ./)
  docker run --rm -ti -e OWNERID=${OWNERID} -v `pwd`/ci:/target quay.io/fravi/ci-imagebuilder:latest
  ```


## step 1: extract tag and version from git
./ci/10_getProjectVersion.sh

will create 3 files that can be read ( ie by jenkins to generate
build informations )
- build_vars/tag.name # this file will contain generated version name
- build_vars/branch.name # this file will contain generated version name
- build_vars/version.name # this file will contain generated version name
- build_vars/build.results # this file will contain repository-digest (as $IMAGE_DIGEST) after image push to repository

  to read those files with jenkins use readfile :  
  `def tagname = readFile 'build_vars/tag.name'`

  ## step 2:
  build the image  
  `$$$ARGS$$$ ./ci/30_buildImage_v2.sh`  
  ie:   
  `DOCKER_REPO_ARR="b1repo-docker.vprod.fvigotti.net:8089/fraimport,b2repo-docker.vprod.fvigotti.net:8089/fraimport" PUSH=1 IMAGENAME="images-ci-scripts" ./ci/30_buildImage_v2.sh`


| ARGS| defined as env vars                                                                                                                  |
 |--------------------------------------------------------------------------------------------------------------------------------------|---|
| $DOCKER_BUILD_OPTS | additional build options, ie: DOCKER_BUILD_OPTS="--network=host --compress --squash"                                                 |
| $IMAGENAME | imagename ( default to directory name )                                                                                              |
| $TAG_OVERRIDE | tag  , will override git label, using prodived tag instead (ie : to manually override a previous build tag)                          
| $PUSH                        | push the image to repository after build ( excluding default :latest or :master if on dev/master branch , including all other tags ) |
| $PUSH_LATEST                        | push the latest image if on dev branch                                                                                               |
| $PUSH_MASTER                        | push the master image to repository after build  if on master branch                                                                 |
| $PUSH_LATEST_ONBRANCH                        | regexp for dev branches, if matches will push as :latest  , ie : `"^dev.*\$"`                                                        |
| $DOCKERFILE | full path of the dockerfile, default to `$projectDir/src/Dockerfile` , fallback to `$projectDir/Dockerfile`                          |  
| $REFRESH_BASEIMAGE | default 1 , if 1 will attempt to pull the base image `FROM` from specified Dockerfile before  building                               |  
| ${DOCKER_NETBUILD_OPTIONS:-} | the same as $DOCKER_NETOPTIONS , but used in `docker build` commands ( require docker >= 1.13 )                                      

### additional configuration avilable for `.build_env.sh` file
| ARGS| defined as env vars |
 |---|---|
| any variable | which is ridden from defaults in 05_SHARED_LIB.sh , 10_getProjectVersion.sh ,  50_buildImage.sh |
| $TAG_AUTOTAG | will overwrite autotag ( nb : fully static , so be sure to not conflict final image tag ) |

| $TAG_AUTOTAG_PREFIX | default postfix for autotag is "" , - is the placeholder for empty-field no default  |
| $TAG_AUTOTAG_POSTFIX | default prefix for autotag is "" , - is the placeholder for empty-field no default |
| $TAG_AUTOTAG_TIME | default time for autotag is "date +"%Y-%d-%m-%H%M-%S" "  , - is the placeholder for empty-field no default |
| $TAG_AUTOTAG_BRANCH | will avoid using current full branch name  , - is the placeholder for empty-field no default |


### to update those scripts:
from project directory
```bash
# docker tags :  
# :master - to get last master
# :latest - to get last dev 
# :vxxx - to get specific version 
 

docker pull quay.io/fravi/ci-imagebuilder:master
OWNERID=$(stat -c '%u' ./)
docker run --rm -ti -e OWNERID=${OWNERID} -v `pwd`/ci:/target quay.io/fravi/ci-imagebuilder:master
```

# external configuration
in project dir  a _ci_override.sh script can contain override for ci scripts automations,
- $FRA_CI_IMAGE_NAME   
  if defined can contain different ci image for selfupdate , ie quay.io/fravi/ci-imagebuilder:dev instead of quay.io/fravi/ci-imagebuilder:master
