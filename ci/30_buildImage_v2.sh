#!/usr/bin/env bash
set -xe


export SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export PROJECT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )
export BASENAME=$(basename $PROJECT_DIR)

### ALWAYS LOWERCASE THE PROJECT NAME ( when is taken from the project directory)
if [ -z "$PROJECT_NAME" ]; then
  export PROJECT_NAME=$(basename $PROJECT_DIR | tr '[:upper:]' '[:lower:]')
fi

. $SCRIPT_DIR/05_SHARED_LIB.sh

## ACCEPTED ENV VARIABLES :
export BUILDAHBIN="${BUILDAHBIN:-"buildah"}"

# https://stackoverflow.com/questions/10586153/how-to-split-a-string-into-an-array-in-bash
### split array DOCKER_REPO_ARR  using separator ","
IFS=',' read -r -a DOCKER_REPO_ARR <<< "$DOCKER_REPO_ARR"


[[ ! -n "${DOCKER_REPO_ARR[*]}" ]] && {
 echo "[INFO] $DOCKER_REPO_ARR not defined, initializing over DOCKER_REPO=$DOCKER_REPO"
 export DOCKER_REPO_ARR=("$DOCKER_REPO")
}

export PUSH=${PUSH:-0} # IF 1 , image will be pushed to docker repository
export PUSH_LATEST=${PUSH_LATEST:-0} # IF 1 , image will be pushed to docker repository overriding latest if on dev branch
export PUSH_LATEST_ONBRANCH=${PUSH_LATEST_ONBRANCH:-""} # regexp that will trigger push latest if matches git branch
export PUSH_MASTER=${PUSH_MASTER:-0} # IF 1 , image will be pushed to docker repository overriding master if on master branch

export AFTERBUILD_CLEANIMAGE=${AFTERBUILD_CLEANIMAGE:-1} # IF 1 , image will be deleted after push
export IMAGENAME=${IMAGENAME:-$BASENAME} # IF 1 , image will be pushed to docker repository

export IMAGENAME=${IMAGENAME:-PROJECT_NAME} # IF 1 , image will be pushed to docker repository

export REFRESH_BASEIMAGE=${REFRESH_BASEIMAGE:-1} # IF 1 , image will be pushed to docker repository
export TAG_VERSION=$(getCurrentTag)
export TAG_AUTOTAG=${TAG_AUTOTAG:-}


echo "[INFO] default TAG_AUTOTAG = $TAG_AUTOTAG"

if [ -z "$TAG_AUTOTAG" ]; then
  getAutoTag "resultsAutotag"
   export TAG_AUTOTAG=$resultsAutotag
   echo "[INFO] generated autotag = $TAG_AUTOTAG"
fi

if [ -z "$TAG_VERSION" ]; then
   echo "[INFO] overridden tag version with TAG_AUTOTAG=$TAG_AUTOTAG"
  export  TAG_VERSION="$TAG_AUTOTAG"
fi


if [ -z "${DOCKERFILE}" ]; then
  export DOCKERFILE="${PROJECT_DIR}/src/Dockerfile" # default to /src/Dockerfile
  if [ ! -f "${DOCKERFILE}" ] && [ -f "${PROJECT_DIR}/Dockerfile" ]; then # but if not exist
    export DOCKERFILE=$PROJECT_DIR"/Dockerfile" ## default to /Dockerfile
  fi
fi

if [ ! -f "${DOCKERFILE}" ]; then
  echo "[FATAL] DOCKERFILE NOT FOUND at path : ${DOCKERFILE}"
  exit 1 ;
fi

## enter the directory of the dockerfile ( all COPY / ADD / .. command should reference from Dockerfile location )
cd $(dirname "${DOCKERFILE}")


if [ -z "${IMAGENAME}" ]; then
  echo " please provide imagename! "
  exit 1;
fi

DOCKER_BUILD_OPTS=${DOCKER_BUILD_OPTS:-} # ie : --network=host

initBuildResultsFile

function do_refresh_base_image(){
  local -r BASEIMAGE=$(cat ${DOCKERFILE}  | egrep "^FROM" | cut -d' ' -f 2)
  if [ ! -z "${BASEIMAGE}" ]; then
   $BUILDAHBIN pull "${BASEIMAGE}" || echo "[WARNING] error refreshing base image ${BASEIMAGE} before build "
  fi
}
 [[ ${REFRESH_BASEIMAGE} -eq 1 ]] && do_refresh_base_image


function do_build(){
  local -r IMAGETAG=$1
  $BUILDAHBIN build  --build-arg=COMMIT="$(getShortCommitHash)" --build-arg="BRANCH=$(getCurrentBranch)"  ${DOCKER_NETBUILD_OPTIONS:-} ${DOCKER_BUILD_OPTS} -t "${IMAGETAG}" -f "${DOCKERFILE}" .
  echo "[BUILT]/do_build   ${IMAGETAG}"
}

function do_push_multirepo(){
  local -r SRCIMAGETAG=$1

  local -r IMAGENAME=$2
  local -r TAG=$3

  echo "[MULTIREPO] repo arr = ""${DOCKER_REPO_ARR[*]}"
  ## initialize repo array if not provided
  [[ ! -n ${DOCKER_REPO_ARR[*]} ]] && {
   echo "[INFO] $DOCKER_REPO_ARR not defined, initializing over DOCKER_REPO=$DOCKER_REPO"
   export DOCKER_REPO_ARR=("$DOCKER_REPO")
  }

  for _iterREPO in "${DOCKER_REPO_ARR[@]}"; do
    echo "[PUSHING] image to repo $_iterREPO "   "${IMAGENAME}" "${TAG}"
    do_push "${SRCIMAGETAG}" "${_iterREPO}" "${IMAGENAME}" "${TAG}"
  done

  }
function do_push(){
  local -r SRCIMAGETAG=$1
  local -r DST_REPO=$2
  local -r IMAGENAME=$3
  local -r TAG=$4

  _full_image="${DST_REPO}/${IMAGENAME}:${TAG}"
  $BUILDAHBIN push "${SRCIMAGETAG}" "$_full_image"
  echo "[PUSHED]" $_full_image
  #appendBuildResults "IMAGE_TAG_NAME" "${TAG}" # ie IMAGE_TAG_NAME=latest
  #appendBuildResults "IMAGE_TAG_${TAG}_DIGEST" "$(getBuildRepoFullDigest $_full_image)" # IMAGE_TAG_latest_DIGEST=$DIGEST ( not valid because tagname could contain invalid char for variable name ( ie : '.' ))

  ## READ  IMAGE DIGEST FROM REMOTE REPOSITORY
  IMG_DIGEST=""
  getBuildRepoFullDigest $_full_image IMG_DIGEST

  appendBuildResults "IMAGE_DIGEST" "$IMG_DIGEST"
  appendBuildResults "IMAGE_NAME" "$_full_image"

}

BRANCHNAME="$(getCurrentBranch)"
builtTag=""  ## tag of the image built ( so can be used in retaggging )
## if on master branch  build :master and :v$tag
if [ "$BRANCHNAME" = "master" ]; then
 builtTag="master"
 #do_build "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
 do_build  "${IMAGENAME}:$builtTag"
 #[[ ${PUSH} -eq 1 ]] && do_push "${IMAGENAME}:$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
 [[ ${PUSH_MASTER} -eq 1 ]] && do_push_multirepo "${IMAGENAME}:$builtTag" "${IMAGENAME}" "$builtTag"
 #[[ ${PUSH} -eq 1 ]] && do_push_multirepo "${IMAGENAME}:$builtTag" "${IMAGENAME}" "$builtTag"
fi

## if on "dev.*" branch build :latest
if [[ "$BRANCHNAME" =~ ^dev.*$ ]]; then
#if [ "$BRANCHNAME" =~ "dev" ]; then
 builtTag="latest"
 do_build "${IMAGENAME}:$builtTag"
 #[[ ${PUSH} -eq 1 ]] && do_push "${IMAGENAME}:$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
 if [[ ${PUSH_LATEST} -eq 1 ]]; then
   echo "[PUSH_LATEST] enabled"
   do_push_multirepo "${IMAGENAME}:$builtTag" "${IMAGENAME}" "$builtTag"
   else
     if [[ "$BRANCHNAME" =~ $PUSH_LATEST_ONBRANCH ]]; then
      echo "[PUSH_LATEST_ONBRANCH] enabled for branch $BRANCHNAME"
      do_push_multirepo "${IMAGENAME}:$builtTag" "${IMAGENAME}" "$builtTag"
     fi

 fi
 #[[ ${PUSH} -eq 1 ]] && do_push_multirepo "${IMAGENAME}:$builtTag" "${IMAGENAME}" "$builtTag"

 # if git tag is defined build with :$tag
# [[ ! -z "${TAG_VERSION}" ]] && do_retag "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"
# [[ ${PUSH} -eq 1 ]] && [[ ! -z "${TAG_VERSION}" ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "dev-${TAG_VERSION}"

fi

# custom tag with push
if [[ ! -z "${TAG_VERSION}" ]]; then
     writeProjectTagnameDescription "${TAG_VERSION}"
     if [[ -z "$builtTag" ]]; then ## if not yet built with tag
       builtTag="$TAG_VERSION"
       #do_build "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag"
       do_build "${IMAGENAME}:$builtTag"
       # [[ ${PUSH} -eq 1 ]] && do_push "${IMAGENAME}:$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag" && { prependBuildResults "push $DOCKER_REPO" "ok" ; }
     fi

     ## push
     #do_retag "${DOCKER_REPO}" "${IMAGENAME}" "$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"
     #[[ ${PUSH} -eq 1 ]] && do_push "${IMAGENAME}:$builtTag" "${DOCKER_REPO}" "${IMAGENAME}" "${TAG_VERSION}"  && { prependBuildResults "push $DOCKER_REPO" "ok" ; }
     [[ ${PUSH} -eq 1 ]] && do_push_multirepo "${IMAGENAME}:$builtTag" "${IMAGENAME}" "${TAG_VERSION}"  && { prependBuildResults "push $DOCKER_REPO" "ok" ; }

fi

## if on label-XXX branch build :label-XXX branch ( custom branch )
# checking if branch start with .. "label-" , will create a tag with full branch name
#if [[ "$BRANCHNAME" == label-* ]]; then
# do_build "${DOCKER_REPO}" "${IMAGENAME}" "$BRANCHNAME"
# [[ ${PUSH} -eq 1 ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "$BRANCHNAME"
#
# # if git tag is defined build with :$tag
# [[ ! -z "${TAG_VERSION}" ]] && do_retag "${DOCKER_REPO}" "${IMAGENAME}" "$BRANCHNAME" "${DOCKER_REPO}" "${IMAGENAME}" "${BRANCHNAME}-${TAG_VERSION}"
# [[ ${PUSH} -eq 1 ]] && [[ ! -z "${TAG_VERSION}" ]] && do_push "${DOCKER_REPO}" "${IMAGENAME}" "${BRANCHNAME}-${TAG_VERSION}"
#fi

exit 0 ; ## ensure that this is the last errorcode if the program reach this point



